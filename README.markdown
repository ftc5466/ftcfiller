# FTCFiller - 0.2

This is a library for FIRST Tech Challenge teams to use in their Robot Controller apps.

---

# What It Does

When working with motors and sensors, teams may find it redundant to name them in the configuration and the code to then have to use the configuration name again with the hardware map. With this library, motors and sensors can be automatically retrieved using the names they are given in the code.

---

# How It Works

This library uses [reflection](http://docs.oracle.com/javase/tutorial/reflect/index.html) to find all fields in an OpMode annotated with the FillByName annotation. It then uses the OpMode's HardwareMap to get the device configured with the same name as the field. This library also uses reflection to find all DeviceMapping fields in the HardwareMap class. This should allow it to be compatible with extensions to the HardwareMap class without updating it.

---

# Gradle Dependency

### Repository

To add this as a Gradle dependency, first add our Maven repository to your FtcRobotController build.gradle:

```
repositories {
    ...
    maven {
        url 'https://bitbucket.org/ftc5466/maven-repo/raw/master/'
    }
}
```

### Dependency

Then add the dependency:
```
dependencies {
    ...
    compile 'com.ftc5466.ftcfiller:ftcfiller:0.2@aar'
}
```

---

# Sample OpMode

```
public class FillTest extends FillingOpMode {
    @FillByName
    private DcMotor testMotor;
    @FillByName
    private ModernRoboticsI2cGyro gyro;

    @Override
    public void loop() {
        testMotor.setPower(Math.random());
        telemetry.addData("gyro", gyro.getIntegratedZValue());
    }
}
```