/*
 * Copyright (c) 2016 The Combustible Lemons - FTC Team 5466 <http://ftc5466.com>
 *
 * The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.ftc5466.ftcfiller;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.HardwareMap;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Map;

/**
 * Class used for automatically "filling" hardware fields in an {@link OpMode}
 */
public class Filler {
    /**
     * "Fills" all fields annotated with {@link FillByName} in the {@link OpMode}. Objects are
     * retrieved from {@code opMode}'s {@link HardwareMap} using the variable name.
     * @param opMode the {@link OpMode} whose annotated fields are to be "filled"
     * @see #fill(Object, HardwareMap)
     */
    public static void fill(OpMode opMode) {
        fill(opMode, opMode.hardwareMap);
    }

    /**
     * "Fills"  all fields annotated with {@link FillByName} in the given object using the given
     * {@link HardwareMap}. Objects are retrieved from the {@link HardwareMap} using the variable
     * name.
     * @param o the object whose annotated fields are to be "filled"
     * @param hardwareMap the {@link HardwareMap} to retrieve from
     * @see #fill(OpMode)
     */
    public static void fill(Object o, HardwareMap hardwareMap) {
        Field[] fields = o.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(FillByName.class)) {
                Class<?> clazz = field.getType();
                String name = field.getName();
                field.setAccessible(true);
                try {
                    field.set(o, hardwareMap.get(clazz, name));
                } catch (IllegalAccessException ignored) {}
            }
        }
    }
}
